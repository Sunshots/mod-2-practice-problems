import json
def decode_and_pluck(input_json, fields):
    dict = json.loads(input_json)
    return {k: dict[k] for k in fields}
