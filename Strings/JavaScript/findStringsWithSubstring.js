function findStringsWithSubstring(strings, substring) {
    let res = [];
    for (let s of strings){
        if (s.includes(substring)){
            res.push(s);
        }
    } return res;
}
