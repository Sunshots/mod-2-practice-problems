function nonEmptyStrings(strings) {
    let results = [];
    for (let s of strings){
        if (s.length > 0){
            results.push(s);
        }
    } return results;
}
