# def shorter_than_5(strings):
#     results = []

#     for string in strings:
#         if len(string) < 5:
#             results.append(string)

#     return results

def shorter_than(strings, max_length):
    return [string for string in strings if len(string) < max_length]
