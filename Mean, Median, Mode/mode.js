function mode(numbers) {
    const count = {};
    for (let n of numbers){
        if (count[n]) {
            count[n] += 1;
        } else {
            count[n] = 1;
        }
    } return Object.keys(count).reduce((a, b) => count[a] > count[b] ? a: b);
}
